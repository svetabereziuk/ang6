import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login.component';
import { ReactiveFormsModule } from '@angular/forms';
import { MatDialogModule } from "@angular/material";
import { LoginDialogModule } from "./login-dialog/login-dialog.module";
import { LoginDialogComponent } from "./login-dialog/login-dialog.component";

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MatDialogModule,
    LoginDialogModule
  ],
  declarations: [
    LoginComponent
  ],
  exports: [
    LoginComponent
  ],
  entryComponents: [LoginDialogComponent]
})
export class LoginModule { }
