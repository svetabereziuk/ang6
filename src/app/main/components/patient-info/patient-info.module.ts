import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatCardModule } from '@angular/material/card';
import { MatListModule } from '@angular/material/list';

import { PatientInfoDirective } from './patient-info.directive';
import { PatientInfoComponent } from './patient-info.component';

@NgModule({
  imports: [
    CommonModule,
    MatCardModule,
    MatListModule
  ],
  declarations: [
    PatientInfoComponent,
    PatientInfoDirective
  ],
  exports: [
    PatientInfoComponent
  ]
})
export class PatientInfoModule { }
