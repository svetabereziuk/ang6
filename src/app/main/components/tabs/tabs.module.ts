import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TabsComponent } from './tabs.component';

import { MatTabsModule } from '@angular/material/tabs';
import { PatientInfoModule } from '../patient-info/patient-info.module';

@NgModule({
  imports: [
    CommonModule,
    MatTabsModule,
    PatientInfoModule
  ],
  declarations: [
    TabsComponent
  ],
  exports: [
    TabsComponent
  ]
})
export class TabsModule { }