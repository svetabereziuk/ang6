import { NgModule } from '@angular/core';
import { DiagnosesComponent } from './diagnoses.component';

@NgModule({
  imports: [    
  ],
  declarations: [DiagnosesComponent],
  exports:[
    DiagnosesComponent
  ]
})
export class DiagnosesModule { }
