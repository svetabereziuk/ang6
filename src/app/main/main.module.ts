import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainComponent } from './main.component';
import { PatientsContainerModule } from './containers/patients-container/patients-container.module';
import { DiagnosesModule } from './components/diagnoses/diagnoses.module';
import { LoginModule } from './components/login/login.module';

@NgModule({
  imports: [
    CommonModule,
    PatientsContainerModule,
    DiagnosesModule,
    LoginModule
  ],
  declarations: [
    MainComponent
  ],
  exports: [
    MainComponent
  ]
})
export class MainModule { }
