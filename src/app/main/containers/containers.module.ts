import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ViewportModule } from './viewport/viewport.module';


@NgModule({
  imports: [
    CommonModule,
    ViewportModule
  ],
  declarations: []
})
export class ContainersModule { }
