import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TabsModule } from '../../components/tabs/tabs.module';

import { PatientsContainerComponent } from './patients-container.component';

@NgModule({
  imports: [
    CommonModule,
    TabsModule
  ],
  declarations: [
    PatientsContainerComponent
  ],
  exports: [
    PatientsContainerComponent
  ]
})
export class PatientsContainerModule { }