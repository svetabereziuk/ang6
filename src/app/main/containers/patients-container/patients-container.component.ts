import { Component } from '@angular/core';

@Component({
  selector: 'app-patients-container',
  templateUrl: './patients-container.component.html',
  styleUrls: ['./patients-container.component.css']
})
export class PatientsContainerComponent {

  selectedPatient = {
    id: 1,
    email: 'john.doe@gmail.com',
    firstName: 'John',
    lastName: 'Doe',
    sex: 'male',
    birthData: '21.12.1968',
    deathData: '',
    business: 'architector',
    city: 'Lviv',
    street: 'Svobody',
    house: '12',
    zip: '099678',
    phone: '+38(099)-509-88-99',
    workPhone: '+38(099)-509-88-99'
  };
}
