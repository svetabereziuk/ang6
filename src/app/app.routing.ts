import { NgModule } from "@angular/core";
import { RouterModule, Routes } from '@angular/router';
import { TabsComponent } from './main/components/tabs/tabs.component';
import { DiagnosesComponent } from './main/components/diagnoses/diagnoses.component';

const routes: Routes = [
    { path: "overview", component: DiagnosesComponent },
    { path: "patients", component: TabsComponent },
    { path: "admin", component: DiagnosesComponent }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule],
  })
  export class AppRoutingModule { }
  





